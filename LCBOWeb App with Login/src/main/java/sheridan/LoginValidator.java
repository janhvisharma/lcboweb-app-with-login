/*
 * @author Janhvi Sharma 
 */
package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		String pattern ="^[a-zA-z][a-zA-Z0-9]{5,}$";
		return loginName.matches(pattern) && loginName.length( ) >= 6 ;
	}
}
