package sheridan;

/*
 * @author Janhvi Sharma 
 */
import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	// 1. Login name must have alpha characters and numbers only, but should not
	// start with a number

	// Regular
	@Test
	public void testValidLoginCharsRegular() {
		boolean result = LoginValidator.isValidLoginName("janhvi28");
		assertTrue("Valid Login Name", result);
	}

	// Exception with special characters
	@Test
	public void testValidLoginCharsException() {
		boolean result = LoginValidator.isValidLoginName("@!#$&^");
		assertFalse("Invalid Login Name Characters", result);
	}

	// Exception with special characters
	@Test
	public void testValidLoginNumException() {
		boolean result = LoginValidator.isValidLoginName("28janhvi");
		assertFalse("Invalid Login Name Characters", result);
	}

	// Exception with spaces
	@Test
	public void testValidLoginCharsSpaceException() {
		boolean result = LoginValidator.isValidLoginName("janh   28");
		assertFalse("Invalid Login Name Characters", result);
	}

	// Boundary-In
	@Test
	public void testValidLoginCharsBoundaryIn() {
		boolean result = LoginValidator.isValidLoginName("j2anhvi");
		assertTrue("Valid Login Name", result);
	}

	// Boundary-Out
	@Test
	public void testValidLoginCharsBoundaryOut() {
		boolean result = LoginValidator.isValidLoginName("janhv#");
		assertFalse("Invalid Login Name Characters", result);
	}

	// 2. Login name must have at least 6 alphanumeric characters
	
	// Regular
	@Test
	public void testValidLoginLengthRegular() {
		boolean result = LoginValidator.isValidLoginName("sharma12");
		assertTrue("Valid Login Name", result);
	}

	// Exception
	@Test
	public void testValidLoginLengthException() {
		boolean result = LoginValidator.isValidLoginName("  ");
		assertFalse("Invalid Login Name Length", result);
	}

	// Boundary-In
	@Test
	public void testValidLoginLengthBoundaryIn() {
		boolean result = LoginValidator.isValidLoginName("j2nhvi");
		assertTrue("Valid Login Name", result);
	}

	// Boundary-Out
	@Test
	public void testValidLoginLengthBoundaryOut() {
		boolean result = LoginValidator.isValidLoginName("j2");
		assertFalse("Invalid Login Name Length", result);
	}
}
